import React from 'react';
import s from './Ministries.module.css';
import MinistriesNav from './MinistriesNav/MinistriesNav';
import Description from './Description/Description';

const Ministries = () => {
    return (
        <div className={s.container}>
            <MinistriesNav />
            <Description />
        </div>
    );
}

export default Ministries;