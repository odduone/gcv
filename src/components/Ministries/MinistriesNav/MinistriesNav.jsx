import React from 'react';
import s from './MinistriesNav.module.css';
import { NavLink } from 'react-router-dom';
import MinistriesNavButton from './MinistriesNavButton/MinistriesNavButton';

const MinistriesNav = () => {
    return (
        <div className={s.nav}>
            <div>
                <h1>MINISTRIES</h1>
                <p>Click below to learn about some of our church happenings</p>
            </div>
            <MinistriesNavButton value="ADULTS" link="/ministries/adults" />
            <MinistriesNavButton value="YOUNG ADULTS" link="/ministries/young-adults" />
            <MinistriesNavButton value="YOUTH" link="/ministries/youth" />
            <MinistriesNavButton value="KIDS" link="/ministries/kids" />
            <MinistriesNavButton value="NURSERY" link="/ministries/nursery" />
        </div>
    );
}

export default MinistriesNav;