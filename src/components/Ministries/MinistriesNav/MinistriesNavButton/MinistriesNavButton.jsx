import React from 'react';
import s from './MinistriesNavButton.module.css';
import { NavLink } from 'react-router-dom';

const MinistriesNavButton = (props) => {
    return (
        <NavLink to={props.link} activeClassName={s.activeMinistriesNavButton} className={s.ministriesNavButton}>{props.value}</NavLink>
    );
}

export default MinistriesNavButton;