import React from 'react';
import s from './Description.module.css';
import { NavLink } from 'react-router-dom';

const Description = () => {
    return (
        <div className={s.description}>
            <p>Description</p>
        </div>
    );
}

export default Description;