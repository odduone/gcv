import React from "react";
import s from "./Nav.module.css";
import NavButton from "./NavButton/NavButton";

const Nav = props => {
  let navData = [
    { value: "About Us", link: "/about-us" },
    { value: "Our Beliefs", link: "/our-beliefs" },
    { value: "Ministries", link: "/ministries" },
    { value: "Our Elders", link: "/our-elders" },
    { value: "Calendar", link: "/calendar" },
    { value: "Seremons", link: "/seremons" },
    { value: "Seeking God", link: "/seeking-god" },
    { value: "Contact Us", link: "/contact-us" },
    { value: "Give", link: "https://www.canadahelps.org/en/dn/30529" }
  ];

  let navButtonElements = navData.map(navButton => (
    <NavButton value={navButton.value} link={navButton.link} />
  ));

  return <div className={s.nav}>{navButtonElements}</div>;
};

export default Nav;
